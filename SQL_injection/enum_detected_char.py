#!/usr/bin/env python3

import requests


char_to_test = [ "SELECT", "FROM", "NOT", "AND", "OR", "LIKE", "ALL", "WHERE", "UNION", "ORDER", "BY", "ASC", "DESC", "SUBSTRING", "GROUP", "CONCAT"]
char_to_test += [chr(i) for i in range(128)]

#detected_text = "has been detected"
detected_text = "DETECTED"

detected = []

url = "https://secureblog.challenge.operation-kernel.fr/v3/post.php"

for test in char_to_test:
    params = { "id": test}
    res = requests.get(url, params=params)
    if ( detected_text in res.text):
        detected.append(ascii(test))
        print ("Les caractères filtrés sont : " + ' '.join(detected))